#!/usr/bin/env python3


import numpy as np
from PIL import Image, ImageTk  # sudo apt-get install python3-pil python3-pil.imagetk
import sys
from tkinter import Tk, SE, LEFT, BOTH, Canvas, Button, Frame, Menu
from tkinter.filedialog import askopenfilename, asksaveasfilename


class App:
    def __init__(self, maybe_path):
        self.root = Tk()
        self.root.title("PyMP - Python Image Manipulation Program")

        menu = Menu(self.root)
        menu_file = Menu(menu, tearoff=0)
        menu_file.add_command(label="Open...", command=self.open)
        menu_file.add_command(label="Save as...", command=self.save)
        menu_file.add_separator()
        menu_file.add_command(label="Exit", command=self.root.quit)
        menu.add_cascade(label="File", menu=menu_file)
        self.root.configure(menu=menu)

        row1 = Frame(self.root)
        row1.pack(fill=BOTH)

        btn_flip_90 = Button(row1, text="Rotate 90°", command=lambda: self.rotate(3))
        btn_flip_90.pack(side=LEFT)

        btn_flip_180 = Button(row1, text="Rotate 180°", command=lambda: self.rotate(2))
        btn_flip_180.pack(side=LEFT)

        btn_flip_270 = Button(row1, text="Rotate 270°", command=lambda: self.rotate(1))
        btn_flip_270.pack(side=LEFT)

        btn_mirror_h = Button(row1, text="Mirror horizontally", command=self.mirror_h)
        btn_mirror_h.pack(side=LEFT)

        btn_mirror_v = Button(row1, text="Mirror vertically", command=self.mirror_v)
        btn_mirror_v.pack(side=LEFT)

        row2 = Frame(self.root)
        row2.pack(fill=BOTH)

        btn_invert = Button(row2, text="Invert", command=self.invert)
        btn_invert.pack(side=LEFT)

        btn_grayscale = Button(row2, text="Grayscale", command=self.grayscale)
        btn_grayscale.pack(side=LEFT)

        btn_brighten = Button(row2, text="Brighten", command=self.brighten)
        btn_brighten.pack(side=LEFT)

        btn_darken = Button(row2, text="Darken", command=self.darken)
        btn_darken.pack(side=LEFT)

        btn_highlight_edges = Button(row2, text="Highlight edges", command=self.edges)
        btn_highlight_edges.pack(side=LEFT)

        self.canvas = Canvas(self.root)
        self.canvas.pack()

        self.data = None
        self.image = None
        self.photo = None  # for some reason this reference must be kept alive this way or nothing shows up

        if maybe_path is None:
            if not self.open():
                return
        else:
            self.load_image(maybe_path)

        self.root.mainloop()

    def load_image(self, path):
        with Image.open(path) as image_file:
            self.data = np.asarray(image_file, np.uint8)

        self.update()

    def update(self):
        self.image = Image.fromarray(self.data)

        # master is necessary, otherwise the PhotoImage silently attaches itself to the first tkinter object
        # which is not necessarily root, yay for hidden global variables
        self.photo = ImageTk.PhotoImage(self.image, master=self.root)

        w, h = self.data.shape[1], self.data.shape[0]
        self.canvas.configure(width=w, height=h)
        self.canvas.create_image((w + 1, h + 1), image=self.photo, anchor=SE)

    def open(self):
        path = askopenfilename(parent=self.root)
        # apparently tkinter thinks it's ok to return empty string when you close the dialog except the first time
        if path is () or path == '':
            return False
        else:
            self.load_image(path)
            return True

    def save(self):
        path = asksaveasfilename(parent=self.root)
        if path is () or path == '':
            return
        self.image.save(path)

    def rotate(self, k):
        self.data = np.rot90(self.data, k)
        self.update()

    def mirror_h(self):
        self.data = self.data[:, ::-1]
        self.update()

    def mirror_v(self):
        self.data = self.data[::-1]
        self.update()

    def invert(self):
        self.data = 255 - self.data
        self.update()

    def grayscale(self):
        self.data = np.copy(self.data)
        # noinspection PyUnresolvedReferences
        gray = (0.299 * self.data[:, :, 0] + 0.587 * self.data[:, :, 1] + 0.114 * self.data[:, :, 2]).astype(np.uint8)
        self.data[:, :, 0] = gray
        self.data[:, :, 1] = gray
        self.data[:, :, 2] = gray
        self.update()

    def brighten(self):
        self.data = np.minimum(self.data * 1.111, 255).astype(np.uint8)
        self.update()

    def darken(self):
        # noinspection PyUnresolvedReferences
        self.data = (self.data * 0.9).astype(np.uint8)
        self.update()

    def edges(self):
        self.data = np.copy(self.data)
        tmp = 9 * self.data[1:-2, 1:-2].astype(np.int16) \
              - self.data[0:-3, 0:-3] - self.data[0:-3, 1:-2] - self.data[0:-3, 2:-1] \
              - self.data[1:-2, 0:-3] - self.data[1:-2, 2:-1] \
              - self.data[2:-1, 0:-3] - self.data[2:-1, 1:-2] - self.data[2:-1, 2:-1]
        # noinspection PyTypeChecker
        self.data[1:-2, 1:-2] = np.clip(tmp, 0, 255).astype(np.uint8)
        self.update()


def main():
    if len(sys.argv) == 1:
        App(None)
    elif len(sys.argv) == 2:
        App(sys.argv[1])
    else:
        print("usage: pymp.py [image_file]")


if __name__ == '__main__':
    main()
